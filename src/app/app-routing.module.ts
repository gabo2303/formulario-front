import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { RegistrosComponent } from './components/registros/registros.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'index' },
    { path: 'index', component: IndexComponent,  },  
    { path: 'formulario', component: FormularioComponent,  },
    { path: 'registro-formulario', component: RegistrosComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }