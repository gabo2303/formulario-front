import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { RegistrosService } from '../../services/registros/registros.service';
import { Formulario } from 'src/app/models/formulario/formulario.model';

@Component({
    selector: 'app-postulaciones',
    templateUrl: '../../pages/registros/registros.component.html',
    styleUrls: ['../../pages/registros/registros.component.css']
})
export class RegistrosComponent implements OnInit 
{
    formularios: Formulario

    constructor(public router: Router, public registrosService:RegistrosService, ) { }

    ngOnInit(): void {
        this.loadData()
    }

    loadData()
    {
        let vm = this

        this.registrosService.traeRegistros().then
        (
            (registros: any) =>
            {
                console.log(registros)
                vm.formularios = registros
            }
        )
        .catch ( error => console.log(error) )
    }
}