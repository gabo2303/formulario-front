import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { NgForm } from '@angular/forms';

import { FormularioService } from '../../services/formulario/formulario.service';

import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

import { RegistrosService } from 'src/app/services/registros/registros.service';


@Component({
    selector: 'app-becas',
    templateUrl: '../../pages/formulario/formulario.component.html',
    styleUrls: ['../../pages/formulario/formulario.component.css']
})
export class FormularioComponent implements OnInit 
{
    tiposBebidas: any = ['Coca Cola', 'Coca Cola Light', 'Coca Cola Zero', 'Ninguna']

    formulario = { bebida: "", mail: "" }

    constructor(private router:Router, private formularioService:FormularioService, private dialog: MatDialog, 
                private registrosService: RegistrosService) {  }

    ngOnInit(): void { }

    guardar(form: NgForm)
    {        
        let vm = this

        this.formularioService.ingresaFormulario(this.formulario).then
        (
            () =>
            {
                vm.openAlertDialog('Se ha ingresado la encuesta exitosamente', 'btn-success')
                
                form.reset()

                return
            }
        )
        .catch ( error => 
        {
            console.log(error)
            
            vm.openAlertDialog('Ha ocurrido un error al ingresar la encuesta', 'btn-danger')            
        })  
    }

    openAlertDialog(mensaje: string, clase: string)  
    {
        this.dialog.open(AlertDialogComponent, {
            data: { message: mensaje, clase: clase, buttonText: { cancel: 'Listo!' }, }
        })
    }
}