import { Component, OnInit } from '@angular/core';

import { AppService } from '../../services/app.service';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: '../../pages/index/index.component.html',
  styleUrls: ['../../pages/index/index.component.css']
})
export class IndexComponent implements OnInit 
{
    options = { autoClose: true, keepAfterRouteChange: false };
    mensaje: boolean = false;
    mensajeError: string;
    nombreUsuario: string = localStorage.getItem("nombreUsuario");

    constructor(private appService: AppService, public router: Router, private route: ActivatedRoute, ) { }

    ngOnInit(): void {  }
}
