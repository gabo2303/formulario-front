import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class FormularioService 
{
    constructor(public http: HttpClient, ) { }

    ingresaFormulario(formulario: any)
    {
        return this.http.post(`./api/ingresaFormulario`, 
        { "bebida":formulario.bebida, "mail":formulario.mail }, { headers: this.armaCabecera() }).toPromise();        
    }
    
    armaCabecera()
    {
        return new HttpHeaders({            
            'Content-Type':'application/json',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE',
            'Access-Control-Allow-Origin': '*',
        });
    }
}