import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RegistrosService 
{
    constructor(private http: HttpClient) { }
     
    traeRegistros() 
    {
        return this.http.get(`./api/traeFormulariosIngresados`, { headers:this.armaCabecera() }).toPromise();
    }

    armaCabecera()
    {
        return new HttpHeaders({            
            'Content-Type':'application/json',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE',
            'Access-Control-Allow-Origin': '*',
        });
    }
}