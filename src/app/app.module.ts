import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout'; 
import { HttpClientModule } from '@angular/common/http';

import { FormularioComponent } from './components/formulario/formulario.component';
import { RegistrosComponent } from './components/registros/registros.component';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { IndexComponent } from './components/index/index.component';

import { FormularioService } from './services/formulario/formulario.service';
import { RegistrosService } from './services/registros/registros.service';

const isIE = window.navigator.userAgent.indexOf('MSIE ') > -1 || window.navigator.userAgent.indexOf('Trident/') > -1;

@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    IndexComponent,
    RegistrosComponent,
    AlertDialogComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,    
    AngularMaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule, 
    FormsModule, 
    HttpClientModule,     
  ],
  providers: [FormularioService, RegistrosService, ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { } 