# EJERCICIO FORMULARIO ENCUESTA

Para utilizar esta app es necesario tener previamente instalado NodeJs.

Posicionarse en la carpeta del proyecto con cmd y ejecutar el comando npm install para descargar e instalar la carpeta node_modules.

Una vez terminado lo anterior ejecutar el comando npm start o, yarn start (en caso de tener instalado yarn) para que el sistema se levante correctamente, la url de inicio es localhost://4200.